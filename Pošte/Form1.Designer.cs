﻿namespace Pošte
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.izaberiKera = new System.Windows.Forms.ComboBox();
            this.izaberiPostar = new System.Windows.Forms.ComboBox();
            this.startDugme = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // izaberiKera
            // 
            this.izaberiKera.FormattingEnabled = true;
            this.izaberiKera.Items.AddRange(new object[] {
            "Obična seoska džukela",
            "Šar planinac"});
            this.izaberiKera.Location = new System.Drawing.Point(31, 58);
            this.izaberiKera.Name = "izaberiKera";
            this.izaberiKera.Size = new System.Drawing.Size(137, 21);
            this.izaberiKera.TabIndex = 0;
            // 
            // izaberiPostar
            // 
            this.izaberiPostar.FormattingEnabled = true;
            this.izaberiPostar.Items.AddRange(new object[] {
            "Tip 1",
            "Tip 2"});
            this.izaberiPostar.Location = new System.Drawing.Point(31, 112);
            this.izaberiPostar.Name = "izaberiPostar";
            this.izaberiPostar.Size = new System.Drawing.Size(137, 21);
            this.izaberiPostar.TabIndex = 1;
            // 
            // startDugme
            // 
            this.startDugme.Location = new System.Drawing.Point(31, 151);
            this.startDugme.Name = "startDugme";
            this.startDugme.Size = new System.Drawing.Size(137, 23);
            this.startDugme.TabIndex = 2;
            this.startDugme.Text = "Pokreni simulaciju";
            this.startDugme.UseVisualStyleBackColor = true;
            this.startDugme.Click += new System.EventHandler(this.startDugme_Click);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel.Location = new System.Drawing.Point(197, 12);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(476, 476);
            this.panel.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Izaberi psa: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Izaberi poštara: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 495);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.startDugme);
            this.Controls.Add(this.izaberiPostar);
            this.Controls.Add(this.izaberiKera);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Name = "Form1";
            this.Text = "Pošte keRS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox izaberiKera;
        private System.Windows.Forms.ComboBox izaberiPostar;
        private System.Windows.Forms.Button startDugme;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

