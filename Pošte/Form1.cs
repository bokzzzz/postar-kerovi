﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pošte
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void startDugme_Click(object sender, EventArgs e)
        {
            panel.Refresh();
            Graphics grp = panel.CreateGraphics();
            Pen pen = new Pen(Color.Red);
            Simulacija sim = new Simulacija();
            if(sim.simulacija(grp, panel.Height/2, izaberiPostar.Text,izaberiKera.Text) == true)
            {
                MessageBox.Show("Pas je uhvatio poštara!","Pošte keRS",MessageBoxButtons.OKCancel,MessageBoxIcon.Information);
            }
            else MessageBox.Show("Pas nije uhvatio poštara!", "Pošte keRS", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        }
    }
}
