﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pošte
{
    class Ker
    {
        private double pozicija_X, pozicija_Y;
        private double vjerovatnoca = 0.00001, vrijemePom = 0;
        private string tip;
        public Ker(string tip)
        {
            this.tip = tip;
            pozicija_X = 0;
            pozicija_Y = 20;
        }
        public double Pozicija_X { get => pozicija_X; set => pozicija_X = value; }
        public double Pozicija_Y { get => pozicija_Y; set => pozicija_Y = value; }
        public string Tip { get => tip; set => tip = value; }
        private void PozicijaTip1(double vrijeme, double kvant, double posX, double posY, Func<double, double> brzinaFunk)
        {
            double razX = posX - pozicija_X;
            double razY = posY - pozicija_Y;
            double razXX = (razX / (Math.Sqrt(Math.Pow(razX, 2) + Math.Pow(razY, 2))));
            double razYY = (razY / (Math.Sqrt(Math.Pow(razX, 2) + Math.Pow(razY, 2))));

            double brzina = brzinaFunk(vrijeme);

            pozicija_Y += (razYY * brzina * kvant);
            pozicija_X += (razXX * brzina * kvant);
        }
        public void azurirajPoziciju(double vrijeme, double kvant, double posX, double posY)
        {
            if (tip.Equals("Obična seoska džukela"))
                PozicijaTip1(vrijeme, kvant, posX, posY, brzinaTip1);
            else
                PozicijaTip2(kvant, posX, posY, brzinaTip2);
        }
        private double brzinaTip1(double vrijeme)
        {
            if (vrijeme < 5.0)
                return 2.0 * vrijeme;
            else return 10.0;
        }
        private double brzinaTip2(double vrijeme)
        {
            if (vrijemePom < 4.0)
                return 3.0 * vrijemePom;
            else return 12.0;
        }
        private void PozicijaTip2(double kvant, double posX, double posY, Func<double, double> brzinaFunk)
        {
            double razX = posX - pozicija_X;
            double razY = posY - pozicija_Y;
            double razXX = (razX / (Math.Sqrt(Math.Pow(razX, 2) + Math.Pow(razY, 2))));
            double razYY = (razY / (Math.Sqrt(Math.Pow(razX, 2) + Math.Pow(razY, 2))));
            double brzina = brzinaFunk(vrijemePom);
            if (slip() == true)
            {
                vrijemePom = 0;
            }
            pozicija_Y += (razYY * brzina * kvant);
            pozicija_X += (razXX * brzina * kvant);
            vrijemePom += kvant;
        }
        private bool slip()
        {
            Random rnd = new Random();
            double vrijednost = rnd.NextDouble();
            vjerovatnoca += 0.000001;
            if (vrijednost < vjerovatnoca)
            {
                vjerovatnoca = 0.00001;
                return true;
            }
            return false;
        }
    }
}
