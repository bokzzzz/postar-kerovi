﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Pošte
{
    class Postar
    {
        private double pozicija_X, pozicija_Y, pocetno_X;
        private string tip;
        public Postar(string tip)
        {
            this.Tip = tip;
            Pozicija_Y = 0;
            Random rnd = new Random();
            pocetno_X=Pozicija_X = rnd.Next(1, 50);
        }

        public double Pozicija_X { get => pozicija_X; set => pozicija_X = value;  }
        public double Pozicija_Y { get => pozicija_Y; set => pozicija_Y = value; }
        public string Tip { get => tip; set => tip = value; }
        private double pozicijaPoXTip1(ref double time,ref long timeMili)
        {
            if (time <3.0 )
            {
                return pocetno_X; }
            else
            {
                pozicija_X = pocetno_X + (time-3) * 5.0;
                return pozicija_X;
            }
        }
        private double pozicijaPoXTip2(double time)
        {
            pozicija_X = pocetno_X + time * 2.5;
            return pozicija_X;
        }
        public double pozicijaPoX(ref double time,ref long timeMili)
        {
            if (tip.Equals("Tip 1"))
            {
                return pozicijaPoXTip1(ref time,ref timeMili);
            }
            else return pozicijaPoXTip2(time);
        }
    }
}
