﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Pošte
{
    class Simulacija
    {
        public bool simulacija(Graphics gp,float height,string postarTip,string kerTip)
        {
            Pen pen = new Pen(Color.Blue);
            Pen pen2 = new Pen(Color.Red);
            SolidBrush br = new SolidBrush(Color.Blue);
            Postar postar=new Postar(postarTip);
            Ker ker = new Ker(kerTip);
            double timeNew = 0 ;
            double kvant = 0.001;
            long timeMili = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            while (true)
            {
                gp.DrawEllipse(pen, x: ((float)postar.pozicijaPoX(ref timeNew, ref timeMili))*(float)5, y: (height), width: 3, height: 3);
                gp.FillEllipse(br, x: ((float)postar.pozicijaPoX(ref timeNew, ref timeMili)) * (float)5, y: (height), width: 3, height: 3);
                ker.azurirajPoziciju(timeNew,kvant, postar.Pozicija_X, postar.Pozicija_Y);
                gp.DrawEllipse(pen2, ((float)ker.Pozicija_X)* (float)5, height-((float)ker.Pozicija_Y)*5, 3, 3);
                timeNew = (DateTimeOffset.Now.ToUnixTimeMilliseconds() - timeMili)/(float)1000;
                 if (Math.Abs(postar.Pozicija_X - ker.Pozicija_X) < 0.01 && Math.Abs(postar.Pozicija_Y - ker.Pozicija_Y) < 0.01) return true; ;
                if ((postar.Pozicija_X*(float)5) > (height * 2)) return false;
            }
      
        }
    }
}
